#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

__version__ = "0.1"
__author__  = "Marotte"
__licence__ = "GPL"
__status__  = "Experimental"

import sys

try:

    from Reader import Reader
    from Shunter import Shunter
    
except ImportError as e:

    print(str(e), file=sys.stderr)
    print('Cannot find the module(s) listed above. Exiting.', file=sys.stderr)
    sys.exit(99)
    
class Cleread:

    def __init__(self):
    
        self.reader = Reader()
        self.shunter = Shunter()

if __name__ == '__main__':
    
    sys.exit(1)
