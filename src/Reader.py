#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

import sys

try:

    sys.path.insert(0, './src')
    import re
    from pprint import pprint
    from datetime import datetime

except ImportError as e:

    print('Can’t import module: '+str(e), file=sys.stderr)
    sys.exit(99)

class Reader():

    def __init__(self):

        self.talks = []
        self.stds = []
        self.anss = []
        self.leaves = []
        self.lfile = 'leaves' # Leaves file
        self.re_talk = re.compile(r'« [^»]+ [^»]+ [^»]+ »') # Wrong: missing things like ' « foo « bar » baz baz baz » '
        self.re_std = re.compile(r'[A-ZÆÀÄÉÊ][^.!?…]* *[.!?…]')
        self.re_ans = re.compile(r'[-–] [^.!?…]* *[.!?…]')

    def read(self, f):
     
        start_read_time = datetime.now()
        self.raw = f.read().replace('\n',' ').replace(u"\N{FORM FEED}",' ')
        for item in self.re_talk.findall(self.raw):
            item = item[1:-1].strip()
            # ~ print('TALK: '+item)
            self.talks.append(item)
        for item in self.re_std.findall(self.raw):
            item = item.replace('«','').replace('»','')
            # ~ print('STD : '+item)
            self.stds.append(item)
        for item in self.re_ans.findall(self.raw):
            item = item.replace('– ','').replace('- ','').strip()
            item = item.replace('«','').replace('»','')
            # ~ print('ANS : '+item)
            self.anss.append(item)

        self.leaves = self.talks + self.stds + self.anss
        finished_read_time = datetime.now()
        elapsed_time = finished_read_time - start_read_time

        print('Read time: '+str(finished_read_time-start_read_time), file=sys.stderr)
        print('  '+str(len(self.talks))+' talks', file=sys.stderr)
        print('  '+str(len(self.stds))+' standards', file=sys.stderr)
        print('  '+str(len(self.anss))+' answers', file=sys.stderr)

    def mkleaves(self):
        
        try:
            with open(self.lfile,'r') as f:
                known = f.read()
                for item in known.split('\n'):
                    self.leaves.append(item)
            print('Loaded '+str(len(self.leaves))+' leaves from "'+self.lfile+'"', file=sys.stderr)
        except FileNotFoundError: pass
        print('Adding '+str(len(self.talks + self.stds + self.anss))+' new leaves', file=sys.stderr)
        for item in self.talks + self.stds + self.anss:
            self.leaves.append(item.replace(' => ',''))
        self.leaves = list(set(self.leaves))
        print('Leaves: '+str(len(self.leaves))+' unique leaves', file=sys.stderr)
        
    def writeLeaves(self):
        
        self.mkleaves()
        with open(self.lfile,'w') as f:
            for item in self.leaves: f.write(item+'\n')

