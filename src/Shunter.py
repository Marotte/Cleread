#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

import sys

try:

    sys.path.insert(0, './src')
    import re
    from pprint import pprint
    from datetime import datetime

except ImportError as e:

    print('Can’t import module: '+str(e), file=sys.stderr)
    sys.exit(99)

class Shunter():

    def __init__(self):
    
        self.leaves = []
        self.nodes = []
        self.pfile = 'points'
        self.failed = {}
        self.load()
        
    def load(self):
        """Load “points” (nodes & leaves) from file."""
        with open(self.pfile,'r') as f:
            
            for line in f:
                if line.strip() is '': continue
                if line.strip()[0] is '#': continue
                split = line.split(' => ',1)
                if len(split) > 1: 
                    self.nodes.append(list(map(str.strip,split)))
                else: 
                    split = line.split(' ## ',1)
                    if len(split) > 1:
                        left = split[0].strip()
                        right = split[1].strip()
                        for item in right.split(','):
                            # (left) (.*) *\?? → right \2
                            self.nodes.append(([r'('+left+') (.*) *\??', item.strip()+r' \2']))
                    else:
                        split = line.split(' %% ',1)
                        if len(split) > 1:
                            left = split[0].strip()
                            right = split[1].strip()
                            for item in right.split(','):
                                # (.*) (left) *\?? → right \1
                                self.nodes.append(([r'(.*) ('+left+') *\??', item.strip()+r' \1']))
                        else:
                            split = line.split(' !%% ',1)
                            if len(split) > 1:
                                left = split[0].strip()
                                right = split[1].strip()
                                for item in right.split(','):
                                    # (.*) (left) *\?? → \1 right
                                    self.nodes.append(([r'(.*) ('+left+') *\??', r'\1 '+item.strip()]))
                            else:
                                split = line.split(' !## ',1)
                                if len(split) > 1:
                                    left = split[0].strip()
                                    right = split[1].strip()
                                    for item in right.split(','):
                                        # (left) (.*) *\?? → \2 right
                                        self.nodes.append(([r'('+left+') (.*) *\??', r'\2 '+item.strip()]))
                                else:
                                    self.leaves.append(split[0].strip())
        pprint(self.leaves)
        pprint(self.nodes)

    def shunt(self,string):
        try:
            self.matching_leaves = []
            new = string.strip()
            
            print('Processing: '+new, file=sys.stderr)
            for leaf in self.leaves:
                match = re.compile(new).search(leaf)
                if match:
                    self.matching_leaves.append(match.string)
            if len(self.matching_leaves) > 0: return self.matching_leaves
            for node in self.nodes:
                print('NODE:'+str(node))
                self.matching_leaves = []
                regex = re.compile(node[0])
                # ~ print(regex)
                if regex.match(string):
                    new = regex.sub(node[1], string)
                    # ~ print(string+'|'+new)
                    if new:
                        next_node = self.shunt(new)
                        if next_node: return next_node
                else:
                    fail = self.failed.get(node[0],0)
                    self.failed[node[0]] = fail + 1
                    # ~ if self.failed[node[0]] > 3: break
                    print(self.failed[node[0]])

            # No leaf found: return last node’s value
            return [new]

        except RecursionError:
            print('Infinite recursion!', file=sys.stderr)
            return [string]
