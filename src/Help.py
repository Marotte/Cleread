"""Cleread help"""

import sys

def man_general(what = sys.argv[0]):
    """General help"""
    return """
Clever Reader.

Usage: """+what+""" [-h] [-r [input file]] [-a [question]]

Commands:

    -h/--help        Show this help and exit.
    -r/--read        Read given file or stdin.
    -a/--answer      Search a leaf according to the points file and given question.

To show help on a particular command, use `"""+what+""" -h <command>`.
"""


def man(section = 'general'):
    """Show help"""
    if section == 'general': print(man_general())
    else:
        print("\nNo help section named \""+section+"\"")
        print(man_general())
        sys.exit(2)
        

