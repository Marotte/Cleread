#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
"""Clever Reader command line tool."""
import sys

try:

    sys.path.insert(0, './src')
    from Help    import man
    from Cleread import Cleread
    from Cmdline import Cmdline

except ImportError as e:

    print('Can’t import module: '+str(e), file=sys.stderr)
    sys.exit(99)

if __name__ == '__main__':

    cmdline = Cmdline(sys.argv, {'--help':'-h',
                                 '--read':'-r',
                                 '--answer':'-a'})
    print('OPTIONS : '+str(cmdline.options), file=sys.stderr)
    print('TAGS    : '+str(cmdline.tags), file=sys.stderr)
    print('LAST TAG: '+str(cmdline.lastTag()), file=sys.stderr)
    print('\n=====================================================', file=sys.stderr)

    try:
        if cmdline.option('h'):
            man()
            sys.exit(2)
        cleread = Cleread()
        if cmdline.option('r'):
            try:
                with open(cmdline.options['r'],'r') as f:
                    print('Reading file "'+cmdline.options['r']+'"', file=sys.stderr)
                    cleread.reader.read(f)
                    cleread.reader.writeLeaves()
            except FileNotFoundError as e:
                print(str(e), file=sys.stderr)
                sys.exit(1)
            except TypeError:
                print('Reading stdin…', file=sys.stderr)
                cleread.reader.read(sys.stdin)
                cleread.reader.writeLeaves()
        if cmdline.option('a'):
            try:
                print('Answering: '+cmdline.options['a'], file=sys.stderr)
                cleread.reader.mkleaves()
                cleread.shunter.leaves = cleread.reader.leaves
                print(cleread.shunter.shunt(cmdline.options['a']))
            except TypeError:
                man()
                sys.exit(2)

    except KeyError: pass

    sys.exit(0)
